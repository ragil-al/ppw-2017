from django.test import TestCase, Client


class Lab6UnitTest(TestCase):

    def test_lab_6_url_is_exist(self):
        response = Client().get('/lab-6/')
        self.assertEqual(response.status_code, 200)
