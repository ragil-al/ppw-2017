from django.http import HttpResponseRedirect
from django.shortcuts import render
from lab_2.views import landing_page_content
from .models import Message
from .forms import MessageForm


author = 'Ragil Al Badrun Pasaribu'

about_me = [
    'Sibolga',
    'Motor',
    'Voli',
    'Lapar',
    'Kurus',
    'Kost',
]

def index(request):
    response = {
        'author': author,
        'content': landing_page_content,
        'about_me': about_me,
        'message_form': MessageForm
    }
    html = 'lab_4/lab_4.html'
    return render(request, html, response)


def message_post(request):
    form = MessageForm(request.POST or None)
    if(request.method == 'POST' and form.is_valid()):
        name = request.POST.get('name')
        email = request.POST.get('email')
        message = request.POST.get('message')

        response = {
            'name': name if name != '' else 'Anonymous',
            'email': email if email != '' else 'Anonymous',
            'message': message,
            'author': author
        }

        message = Message(
            name=response.get('name'),
            email=response.get('email'),
            message=response.get('message')
        )
        message.save()
        html ='lab_4/form_result.html'
        return render(request, html, response)
    else:
        return HttpResponseRedirect('/lab-4/')

def message_table(request):
    message = Message.objects.all()
    response = {
        'author': author,
        'message': message
    }
    html = 'lab_4/table.html'
    return render(request, html, response)
